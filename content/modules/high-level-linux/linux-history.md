---
title: "Linux History"
date: 2020-11-11T23:52:25-05:00
draft: false
Weight: 2
---
```sh
    user@linux:~$ history
```
## Overview
Linux has been in development since 1991, but has roots starting in 1965 at Bell Labs with the development of Multics. Multics developers created a simpler operating system UNIX, which was then packaged into the Linux kernel by Linus Torvalds in the early 90's.

### Multics
Multiplexed Information and Computing Service, or _Multics_, was a project created by MIT, Bell, and GE. Famously written in the high level language PL/I, Multics was an unprecedented idea in 1965.

### UNIX
UNIX is the foundation for operating systems such as macOS, BSD, and Linux. Developed at Bell Labs, UNIX is a product of Multics developers, who broke off to create their own operating system. This operating system includes a great deal of popular Linux commands, including grep, the pipe, ls, mv, and cp.

### Linux

The product of Finnish software developer, Linus Torvalds, Linux was started in 1991. Originally named FREAX, Linux received its name from Ari Lemmke once releasing Linux to the public on the University of Helsinki FTP server. In 1992 Linux version 0.12 was released with the [GPL](../linux-licenses) license. In 1993, the first Linux distribution, Slackware Linux, was released. In 1997 and 1998 Gnome and KDE, two of the most popular desktop environments, were released.

---

#### References

[multicians.org - History](https://www.multicians.org/history.html)

[Cloud Savvy IT - A Brief History of UNIX](https://www.cloudsavvyit.com/6066/a-brief-history-of-unix/)

[It's FOSS - Timeline Of The Most Important Events In 25 Years Of Linux](https://itsfoss.com/25-years-linux/)

[Computer Hope - FREAX](https://www.computerhope.com/jargon/f/freax.htm)

[linux.org - About Linus Torvalds _2004_](https://web.archive.org/web/20040626044423/http://www.linux.org/info/linus.html)