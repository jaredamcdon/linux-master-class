---
title: "Linux Licenses"
date: 2020-11-12T01:03:02-05:00
draft: false
weight: 3
---
```sh
    user@linux:~$ vim LICENSE
```

## Licensing and Linux
Linux was originally built upon the GNU General Public License, and has remained so indefinitely. While the software is free and open source, putting software in the _Public Domain_ is not recommended. Software in the _Public Domain_ allows anyone to modify the code and sell it as proprietary software, going against the open source nature of Linux. To prevent free software from going into the Public Domain there is a wide variety of free licenses to classify software under. This provides protections such as copyleft and the ability to fight for software from a legal standpoint.

### Copyleft
Copyleft, both in name and design is the opposite of a copyright. Copyleft provides a form of license that requires all derivative works to also be released as free software.

### GNU Public License
>Nobody should be restricted by the software they use. There are four freedoms that every user should have:  
> * the freedom to use the software for any purpose,
> * the freedom to change the software to suit your needs,
> * the freedom to share the software with your friends and neighbors, and
> * the freedom to share the changes you make.  


| License | Shorthand | Release |
| --- | --- | --- |
| GNU General Public License, version 1 | GPLv1 | February  1989 |
| GNU General Public License, version 2 | GPLv2 | June 1991 |
| GNU General Public License, version 3 | GPLv3 | June 2007 |
| GNU Lesser General Public License, version 2.1 | LGPLv2.1 | February 1999 |
| GNU Lesser General Public License, version 3 | LGPLv3 | June 2007 |
| GNU Affero General Public License, version 3 | AGPLv3 | November 2007 |
| GNU Free Documentation License, version 1.1 | FDLv1.1 | March 2000 |
| GNU Free Documentation License, version 1.2 | FDLv1.2 | November 2002 |
| GNU Free Documentation License, version 1.3 | FDLv1.3 | November 2008 |


### MIT License
>The MIT License removes copyleft and many other features from GNU GPL's. This makes the software more easily malleable by individuals and organizations.

---
#### MIT License Example

Copyright {YEAR} {COPYRIGHT HOLDER}   

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:  
  
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.  
  
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

---

### Mozilla Public License
>The Mozilla Foundation's Mozilla Public License 2.0 (MPL 2.0) sits between the GNU GPL and MIT License. The MPL 2.0 Provides copyleft without the full force of the GNU GPL license.
#### Why MPL?
* File-level copyleft
* Minimal restrictions to modified code
* Allows subsequent projects to use any license the developer sees fit

### Other Licenses
>GNU, MIT, and the Mozilla Foundation provide prominent, but not all encompassing licenses for open source software.
* BSD
* Apache License
* Intel Open Source License
* X11 License

---
#### References
[Opensource.com - Shedding Light on foggy GPL licenses](https://opensource.com/article/17/11/avoiding-gpl-confusion)

[GNU - Licenses](https://www.gnu.org/licenses/licenses.en.html)

[The MIT License - Opensource.org](https://opensource.org/licenses/MIT)

[Mozilla - MPL 2.0 FAQ](https://www.mozilla.org/en-US/MPL/2.0/FAQ/)