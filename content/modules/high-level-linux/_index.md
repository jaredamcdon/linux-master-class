---
title: "High Level Linux"
date: 2020-11-10T00:38:41-05:00
draft: false
weight: 2
---

## What is Linux?

Linux is the underlying kernel for - and just one part of - the _Linux operating system_. Linux installations include a bootloader, daemons, a shell, graphical system and more. 
## Who uses Linux?

Linux is used by programmers, scientists, tech enthusiasts and security professionals, to name a few! Linux is also the most popular server operating system in the world, powering companies big and small to host databases, file servers, web servers and much more.
## Why Linux?
Linux is _FOSS_ Free Open Source Software. Linux is open to all with no license cost, high efficiency, and a malleable base giving users the ability to configure as much or as little as they would like. Linux also has strong support for coding languages and environments, as well as server software and FOSS software for desktop users.

---
#### References
[howtogeek.com - “Linux” Isn’t Just Linux](https://www.howtogeek.com/177213/linux-isnt-just-linux-8-pieces-of-software-that-make-up-linux-systems/)