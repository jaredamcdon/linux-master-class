---
title: "Unix Philosophy"
date: 2020-11-11T22:22:11-05:00
draft: false
weight: 1
---
```sh
    user@linux:~$ traceroute unix.com
```

## What is the UNIX Philosophy?
The UNIX Philosophy comes from a guide written by Doug McIlroy in 1978 stating:
1. Make each program do one thing well. To do a new job, build afresh rather than complicate old programs by adding new “features”.

2. Expect the output of every program to become the input to another, as yet unknown, program. Don’t clutter output with extraneous information. Avoid stringently columnar or binary input formats. Don’t insist on interactive input.

3. Design and build software, even operating systems, to be tried early, ideally within weeks. Don’t hesitate to throw away the clumsy parts and rebuild them.

4. Use tools in preference to unskilled help to lighten a programming task, even if you have to detour to build the tools and expect to throw some of them out after you’ve finished using them.

### Why Does this Matter?
Linux software, especially at the terminal is lightweight and efficient. Software designed for Linux almost always falls under the UNIX philosophy - allowing users to understand how to work with programs and use that data to serve to other programs. Linux does not have one _must have_ tool, but hundreds that work in tandem to increase user and program efficiency.

### What does the UNIX Philosophy Look Like in Action?
```sh
    user@linux:~$ ls files/* | grep lab-report > lab-reports.txt && cat lab-reports.txt
    files/lab-report-1
    files/lab-report-2
```
I can run that all from one line?
* Yes, you can!

How Does it Work?
 * `ls files/*`
    * List all files in the files/ directory
 * `|`
    * Funnel output of `ls files/*` to `grep lab-report`
 * `grep lab-report`
    * Find every instance of "lab-report"
 * `> lab-reports.txt`
    * Redirect `grep lab-report` output to the file "lab-reports.txt"
 * `&&`
    * Also run next command
 * `cat lab-reports.txt`
    * Print the contents of lab-reports.txt
---
#### References
[Medium - Philosophy of UNIX Development](https://medium.com/ingeniouslysimple/philosophy-of-unix-development-aa0104322491)

[The Art of Unix Programming](http://catb.org/~esr/writings/taoup/html/)

[Make Tech Easier - Understanding Pipes and Redirection For the Linux Command Line](https://www.maketecheasier.com/pipes-redirection-for-linux-command-line/)