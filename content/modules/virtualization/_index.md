---
title: "Virtualization"
date: 2022-01-07T01:30:22-05:00
draft: false
weight: 8
---

## Nested Computers

Virtualization is the emulation of computer hardware using an application such as QEMU, Virtbox, or many other solutions. This allows computer hardware to be split up and allow the booting of multiple systems. This can be done to run a server instance without requiring new hardware, or for the use of testing using a different operating system on top of your host.

Linux, and most other operating systems can both act as a host or guest operating system, with the host as the primary operating system and a guest under the hypervisor. When a virtualization application, also known as a hypvervisor, is running, it will take resources from the system such as CPU cores, RAM, USB host devices and more. A virtual machine will also require permanent allocation of disk space to store the bootloader, operating system, user files and applications.

## Virtualized Servers

A common use case for virtualization is to run server applications, such as websites, databases, back-end applications and more, Using virtualization provides a simpler route to scaling out server-side applications due to better defined configuration and deployment in comparison to bare metal hardware. Virtual servers provide a set of configuration files and images that can be more quickly replicated across multiple systems and even can be backed up and redeployed more easily than bare metal instances. 

### On-Prem

Virtualization when on-prem, or in your own datacenter can allow you to buy less servers, licenses, and speed up deployment. This can be done on Windows Server, BSD, but most commonly on Linux. As a system administrator you can elect to run this on any Linux distribution, but specialized distros such as ProxMox provide out of the box tools to easily manage virtualized servers.

### In the Cloud

One primary use-case of the cloud is virtualized servers, allowing organizations and individuals to quickly spin up and tear down new instances of operating systems to test and deploy different applications with high availability. Cloud engineers can also pre-define variables that can cause servers to scale out horizontally depending on user demand and server utilization. All major cloud providers offer virtualized servers with a host of different Linux and non-Linux operating systems 

## Virtualized Networking

Virtualization comes with its own networking configuration in comparison to the host operating system. This allows you to segment and define different networking configurations to best align with your security and availability requirements. For example, if your network runs on 192.168.1.0/24, you can host a virtual network on 10.0.0.1/28 to prevent unrestricted access to your virtual machines.
