---
title: "Virtualization Distros"
date: 2022-01-07T18:19:18-05:00
draft: false
weight: 2
---
```sh
    user@proxmox:~$ qemu-img convert old.vhd -O qcow2 new.qcow2
```