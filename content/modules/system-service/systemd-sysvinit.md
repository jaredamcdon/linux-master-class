---
title: "Systemd and Sysvinit"
date: 2021-01-30T12:17:57-05:00
draft: false
weight: 1
---
```
    user@linux:~$ sudo systemctl start sddm.service
```
## Similar Software?

### Similarities

Systemd has been a source of controversy since gaining main stream usage by Linux distributions, replacing the former top init system, SysV-init. Both pieces of software are designed to be the first process started by linux at boot, and manages the booting process as well as programs and services while the system is running. This makes the init system vital to an operating system, which is what is handled by the core init programs, systemd and SysV-init. Both systemd and SysV-init start system services, load drivers, bring up network interfaces and more at startup, allowing for the system to reach a usable state. Systemd even has been designed to accept init scripts used by SysV-init, further allowing compatibility between the to service managers.

### Differences

SysV-init and systemd diverge after core features, where SysV-init ends its feature set and systemd compiles more features on top. Systemd is able to intelligently launch services based on triggers, manage logins, journaling and much more. Many critics of systemd find that systemd has grown to the point of a software suite rather than a single application or tool, the first rule of the [UNIX philosophy](/modules/high-level-linux/unix-philosophy/).

While SysV-init may look like a small in comparison to the explosion of popularity systemd has seen, many distributions still rely on SysV-init, and many "protest distros" intentionally take Linux distributions using systemd and repackage the software with SysV-init. Most notably, MX Linux uses SysV-init with systemd-shim, providing SysV-init for the init system and systemd-shim to manage most systemd processes without init service.

### Commands

systemd and SysV-init use similar commands with similar syntax, likely due to systemd being developed as a replacement to SysV-init. The majority of system and service commands require `systemctl` as a prefix to the command for systemd, while SysV-init may require `service` or `chkconfig` as a prefix to commands. In general, systemd commands are often more verbose than SysV-init.

| Command usage | systemd syntax | SysV-init syntax |
| --- | --- | --- |
| Start service (x) | systemctl start x.service | service x start |
| Stop service (x) | systemctl stop x.service | service x stop |
| Restart service (x) | systemctl restart x.service | service x restart |
| Start (x) service at boot | systemctl enable x.service | chkconfig x on |
| Halt system | systemctl halt | halt |
| Restart system | systemctl reboot | reboot|
| Check runlevel | systemctl get-default | runlevel |

---

#### References

[PC World - Meet systemd, the controversial project taking over a Linux distro near you](https://www.pcworld.com/article/2841873/meet-systemd-the-controversial-project-taking-over-a-linux-distro-near-you.html)

[MX Linux - About Us](https://mxlinux.org/about-us/)

[UConn UITS Linux Team - systemd commands, hints and cheatsheet](https://linux.uits.uconn.edu/2016/04/15/systemd-commands-hints-and-cheatsheet/)

[LinOxide - Systemd vs Init Cheatsheet for Linux](https://linoxide.com/linux-command/systemd-vs-sysvinit-cheatsheet/)