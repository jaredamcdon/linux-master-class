---
title: "System and Service Managers"
date: 2020-12-20T00:02:43-05:00
draft: false
weight: 6
---

## Overview
System and service managers have been a point of contention for Linux administrators, distribution maintainers, and the greater Linux community as a whole. systemd is the most popular system and service manager, but SysVinit has a fair amount of market share, with certain distribution forks only replacing systemd for SysVinit. A system and service manager is vital to launching and managing services and applications, as the system and service manager is the first process after the kernel loads. To start and stop processes, power off and reboot systems and more, a system and service manager is the encompassing management tool. The system and service manager is always under the process ID 1 from both a theoretical and low-level management perspective.

### Popular System and Service Managers

| Name | Pros | Cons |
| --- | --- | ---|
| Systemd | Most popular, easy to use | Bloated, too much control |
| SysVinit | Simple, fast, Unix-like | Less features, scripting volatility |
| OpenRC | Built on top of SysVinit | Lack of support |
| runit | Clean process states | Outdated development |
| Minit | Efficient speed and size | Little documentation |

----
#### References

[Slant - What is the best alternative to systemd?](https://www.slant.co/options/12956/alternatives/~systemd-alternatives)

[Arch Linux Wiki - SysVinit](https://wiki.archlinux.org/index.php/SysVinit)

[Gentoo Wiki - OpenRC](https://wiki.gentoo.org/wiki/OpenRC)

[Smarden - runit](http://smarden.org/runit/)

[Linux Journal - Minit](https://www.linuxjournal.com/content/look-minit)

