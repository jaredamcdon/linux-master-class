---
title: "Native Installations"
date: 2020-11-09T22:56:02-05:00
draft: false
weight: 1
---
```sh
    user@linux:~$ sudo apt install
```
## Where to start?
>How to choose the best option for you
### Use cases
* Desktop
    * Great first choice
    * Comes with graphical interface and tools
    * Uses more resources
* Server
    * Resource efficient
    * Great for advanced Users
    * Difficult to start with

### Installation steps
>How to quickly get started on Linux
* Go to the website of a Linux distribution
* Download the .iso file
* Burn the .iso to USB, CD, or DVD
    * _Rufus_ is a great FOSS application to burn .iso to USB
* Reboot the system
* Boot to the installation medium and follow the prompts

### Desktop Choices
>The most popular distros
| Name | Based on | Package Manager | Notes |
| --- | --- | --- | --- |
| Debian | N/A | Apt | Intermediate installation, Stable |
| Ubuntu | Debian | Apt | Great for beginners |
| Linux Mint | Ubuntu | Apt | Great for beginners, Windows feel |
| popOS! | Ubuntu | Apt | Great for Gamers |
| Red Hat Enterprise Linux | Fedora | Dnf / Yum | License cost |
| Fedora | N/A | Dnf / Yum | Constant new versions |
| CentOS | Fedora / Red Hat | Dnf / Yum | Stable, Enterprise focused|
| Arch | N/A | pacman | Rolling release, difficult installation | 
| Manjaro | Arch | pacman | More stable Arch, easy installation |
| openSUSE | N/A | Zypper | Stable, lower popularity |

### Server Choices
>The most popular distros
| Name | Based on | Package Manager | Notes |
| --- | --- | --- | --- |
| Debian | N/A | Apt | Intermediate installation, Stable |
| Ubuntu | Debian | Apt | Great for beginners |
| Fedora | Red Hat | Dnf / Yum | Constant new versions |
| CentOS | Fedora / Red Hat | Dnf / Yum | Stable, Enterprise focused|
| openSUSE | N/A | Zypper | Stable, lower popularity |

### These are a lot of option. Whats your picks?

* __Desktop__
    * _Overall_: __Linux Mint__
        * Great stability
        * Easy to use
        * Based off Ubuntu
            * Exceptional documentation and community support
    * _Gaming_: __popOS!__
        * Great for gamers
        * Easy to Use
        * Based off Ubuntu
            * Exceptional documentation and community support
* __Server__
    * _Beginner_: __Ubuntu__
        * Easy to configure
        * Stable
        * Based on Debian
    * _Intermediate/Advanced_: __Debian__
        * Stable to a fault
        * More complicated configuration