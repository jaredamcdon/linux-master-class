---
title: "Windows Subsystem for Linux"
date: 2020-11-09T22:57:02-05:00
draft: false
weight: 3
---
```sh
    user@linux:~$ uname -r
```
## What is Windows Subsystem for Linux?
Windows Subsystem for Linux, or WSL, is a Linux environment directly on Windows. This interacts directly with the host operating system, removing virtual machine overhead and doesn't require you two dualboot.

### How Do I Install WSL?
WSL can be installed by:
* Opening the Microsoft Store
* Searching for a Linux Distribution
    * Example: _Ubuntu_
* Following installation
* Rebooting

### Why Windows Subsystem for Linux?
Many computer enthusiasts like using Linux command line tools that are not available on Windows, but have to use Windows for productivity, professional, and gaming software. WSL Provides a Linux away from Linux so you can have it all.

---
#### References
[Microsoft Documentation](https://docs.microsoft.com/en-us/windows/wsl/about)