---
title: "Virtual Machines"
date: 2020-11-09T22:56:19-05:00
draft: false
weight: 2
---
```sh
    user@linux:~$ VBoxManage list vms
```
## Why a Virtual Machine?
Virtual machines provide a great way to test an operating system, as well as run an operating system alongside your current operating system.

### How Does Linux Work with a Virtual Machine?
After downloading a Linux .iso file, you are able to add it to the virtual CD drive to install linux as if it were native hardware.

### What are the Upsides?
* Using import tools require no manual configuration
* Have two operating systems available at once
* Easily build and rebuild virtual machines on the fly

### What are the Downsides?
* Virtual machines require high performance systems to fully utilize its power
* Virtual machines often struggle to add graphics cards, requiring your processor to display graphics
* Using a virtual machine next to your primary operating system can be cumbersome

### How do I get started?
Install a virtual machine just as any other application

{{<mermaid align = "center">}}
graph LR;
    A[Computer Hardware] --> B[Host OS]
    B --> C[VM Application]
    C --> D[Guest OS]
{{</mermaid>}}