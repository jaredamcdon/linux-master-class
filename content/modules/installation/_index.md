---
title: "Installation"
date: 2020-11-09T22:55:15-05:00
draft: false
weight: 1
---

## How do I get started with Linux?
---
### Install it natively
>Run Linux standalone or dual-booted on a machine.
### Install it with a virtual machine
>Run Linux on top of your main operating system
### Integrate Linux into your Windows instance
>Windows Subsystem for Linux provides a linux shell as an application.

---
### Distribution Overview
| Name | Based on | Package Manager | Server Option? | Notes |
| --- | --- | --- | --- | --- |
| Debian | N/A | Apt | &#9745; __Yes__ | Intermediate installation, Stable |
| Ubuntu | Debian | Apt | &#9745; __Yes__ | Great for beginners |
| Linux Mint | Ubuntu | Apt | &#9744; No | Great for beginners, Windows feel |
| popOS! | Ubuntu | Apt | &#9744; No | Great for Gamers |
| Red Hat Enterprise Linux | N/A | Dnf / Yum | &#9745; __Yes__ | License cost |
| Fedora | Red Hat | Dnf / Yum | &#9745; __Yes__ | Constant new versions |
| CentOS | Fedora / Red Hat | Dnf / Yum | &#9745; __Yes__ | Stable, Enterprise focused|
| Arch | N/A | pacman | &#9745; Yes* | rolling release, difficult installation | 
| Manjaro | Arch | Pacman | &#9744; No | More stable Arch, easy installation |
| openSUSE | N/A | Zypper | &#9745; __Yes__ | Stable, lower popularity |
>*Arch, while can run as a server is not recommended due to stability concerns

---
### What Benefits Does Each Linux Distribution Provide?
_I'm looking for..._
{{<mermaid align="center">}}
graph LR;
    A[Stability] --> B[Debian]
    A --> C[Red Hat]
    A --> D[openSUSE]
    E[Out-of-box Software] -->F[Ubuntu]
    E --> H[popOS!]
    E --> G[Manjaro]
    I[Experimentation] --> J[Arch]
    I --> G[Manjaro]
    L[Security/Hacking] --> M[Kali]
    L --> N[ParrotOS]
{{< /mermaid >}}