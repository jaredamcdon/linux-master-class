---
title: "Server"
date: 2021-01-31T17:17:23-05:00
draft: false
weight: 1
---
```sh
    user@linux:~$ ssh user@10.0.0.5
```

### Headless, Handy, Homogeneous

The Linux terminal is unapologetically pragmatic and efficient. The Linux terminal does not vary between distributions. This makes working between Red Hat Enterprise Linux, Ubuntu, Suse and more very simple. These server distributions vary in their package managers, default applications, and file structure, but otherwise are difficult to tell apart on day to day usage. The Linux server environment will always be a command line shell allowing for text commands to be inputted with outputs sent to the terminal to let the administrator know progress, failure, proper execution or more.

Navigating the terminal means knowing what you need to do and a general idea of how to do it. This can be daunting but with the help of online tutorials, coworkers and friends, and the integrated `--help` flag and `man` command anything is possible. Proper execution at the command line comes down to being able to input commands and synthesize output to properly navigate directories, manage services, and execute software. Once over the hump of using an operating system without a graphical user interface, Linux users and administrators are able to become faster at their work in comparison to desktop applications and graphical servers. Through the use of scripts and not needing to switch between keyboard and mouse, Linux server is able to boost productivity while minimizing resource utilization by the underlying OS.

Many of these assumption assume that there is no graphical interface for the server, which is the most common way to interact with Linux servers. That being said, many server distrobutions provide the ability to use a GUI. This can simplify processes for new and intermediate users, as well as provide additional ways to run software. For example, many server applications with a web interface will use localhost by default, and a GUI will make it easy to modify these settings without complicated networking or modifying configuration files to access from 0.0.0.0.

### Automation, IoT, and Container Images

IOT devices, as referenced earlier, often use Linux as the operating system. This could be done through familiar operating systems such as debian, but will more likely be done with Alpine, Gentoo, or other lesser known operating systems that provide only necessary packages to reduce resource load. Using these stripped down versions of Linux also provide other inherent benefits, such as less overall software. The reduction in software allows for less security vulnerability as there are fewer lines of code that can be exploited.

These Linux distributions often come with their own hurdles, such as limited or no package management. This requires advanced knowledge to update, build, and maintain these images but prove worth it for differently scoped projects. IoT and containerized images also often lack creature comforts such as text editors, networking tools, and even bash, with many relying on the primitive sh shell, which has a more limited usage than bash or similar shells, and could cause problems with scripts designed for more complex shells.