---
title: "Environments"
date: 2021-01-31T13:46:21-05:00
draft: false
weight: 7
---

## Linux - The Underlying Kernel

Linux is well known for its use in servers, desktops, Android, internet of things and much more. All of these devices run Linux. All of these devices are interacted with entirely differently by the user. From an academic perspective pure Linux shows itself in two main environments, Linux on the desktop and server.

When tech enthusiasts think of Linux, they often think of Ubuntu or Linux Mint running on their desktop. Linux does hold some desktop market share and was originally designed to be a desktop operating system. With this said, Linux is most popular on the server and low powered devices such as routers and smart devices. Linux server differs from the desktop by operating cost, efficiency and user experience. Unlike Windows Server, Linux server distributions do not provide a graphical user interface and maximize hardware performance at the cost of having only a terminal to interface with, often without a monitor connected to the device.

While Linux for servers can be vastly different from the desktop experience, the same underlying bash commands and terminal found on a desktop installation function the same way. Desktop Linux only takes the power of server variants and adds creature comforts such as a desktop environment, graphical applications, and support for mice and additional peripherals only necessitated by an individual user.

## The Impact of Distributions

### Differences in Distros
Linux distros have few differences between each other, namely the package manager, kernel version, file structure, and default software. This impacts both server and desktop versions of Linux, changing how you manage networks, install software, and system defaults. With Linux Server installations, you often will have to change your syntax of commands and note different default file locations. For desktop Linux, much more can change. Desktop installations of Linux are impacted in all the ways a server installation is, plus settings in the graphical user interface.

### Desktop Software
Desktop Linux installations come with a display server, a login service, display manager, desktop environment and additional graphical software. All of these are affected by a Linux distribution. Many Linux users compare the difference between Ubuntu and Linux Mint by the differences in desktop environment. While the default desktop environment is an important feature for Linux distributions, what changes the most between desktop distributions is both the underlying software as well as where a build is packaged up or downstream in comparison to other distros, able to affect reliability, security, and features.