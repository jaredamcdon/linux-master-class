---
title: "Desktop"
date: 2021-01-31T17:17:30-05:00
draft: false
weight: 2
---
```
    user@linux:~$ sudo systemctl start sddm.service
```

### Freedom of Choice

Choosing Linux on the desktop can open a wormhole of options, as there are many different distrobutions that often offer similar solutions for the consumer. For example, Pop!_OS, Linux Mint, Zorin, KDE Neon, and Elementary OS are all based upon Ubuntu. They are all designed to make their distribution easier or more user friendly through usage of different package managers, desktop environments, installers, and predefined packages.

When choosing a distribution, it is important to make sure that it meets your needs, which can be hard to define. Almost all Linux distributions are based off of Debian, Redhat, Suse, or Arch which provide different benefits to the user. Debian (and Ubuntu) based operating systems are the most beginner friendly and most common, Redhat offers a closer experience to System Administrators, and Arch-based distributions are favored for their rolling-release package manager.

Package managers are the biggest change you will see between distributions outside of desktop environment. These provide a host of packages that update based on different parameters set by the development teams. Debian uses apt, Arch uses Pacman, Suse uses zypper, and Redhat uses Yum and DNF.

### Deciding on a Look

GNOME, KDE and XFCE are the most popular desktop environments, but there is also Pantheon, Budgie, Cinnamon, MATE and much more. These desktop environments decide the look and feel as well as how much configuration you can modify outside of the box. Desktop environments will also decide a great deal of default applications for basic computer usage, such as your file manager.

Outside of traditional desktop environments, you will also find window managers, a stripped down version of a desktop environment that relies on keybinds and has more customization at the expense of a steeper learning curve. These window managers also provide lower resource utilization out of the box, making it great for users that want as much power out of their system as possible.

### Experimentation

Finding the right linux distro to run on your computer will likely take trial and error to find the best solution for you. As a personal opinion, I believe any distro can work with the right customization, but one that suits your needs out of the box is likely the best fit. Linux is a very different experience from macOS and Windows, and testing new desktops is part of the fun.

----
#### References

[Distrowatch.com](https://distrowatch.com/)