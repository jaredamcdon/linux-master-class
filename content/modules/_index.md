---
title: "Modules"
date: 2020-11-09T22:18:24-05:00
draft: false
weight: 2
---
```sh
    user@linux:~$ cd /
```
## Navigation
---
### [Installation](./installation/)
 * Native installations
 * Virtual machines
 * Windows Subsystem for Linux

### [High Level Linux](./high-level-linux/)
>The theory and design
* Unix Philosophy
* Linux History
* Linux Licenses

### [Basics](./basics)
>Beginning with Linux
* File Systems
* Partition Tables
* File Structure
* Devices and drivers
* User Accounts
* Permissions
* Bash

### [Kernel](./kernel)
>The backbone of the operating system
* Design
* Maintenance
* Development

### [Bootloaders](./bootloader)
>How operating system starts up
* Design
* Bootloaders of Linux
* GRUB

### [System and Service Managers](./system-service) 
>Initializing software after the kernel
* Systemd
* SysVinit

### [Environments](./environments)
>The different use cases of Linux

* Linux Server
	* Server Distributions
* Linux Desktop
	* Desktop Environments
	* Desktop Distributions
	
### [Virtualization](./virtualization)
>Linux as a Host and Guest Operating System
* Hypervisors
* Virtualization Distros
### [Applications]()
>What's an operating system without software?
* Server/CLI Software
    * Package Managers
    * SSH
    * TOP/HTOP
    * VIM/Nano/Emacs
    * Neofetch/Screenfetch
    * Net-Tools
* Desktop/GUI Software
    * GParted
    * Filezilla
* GNU Coreutils
    * Overview
    * Design
    * Utilities