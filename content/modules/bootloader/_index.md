---
title: "Bootloader"
date: 2020-12-06T22:11:42-05:00
draft: false 
weight: 5
---


## Overview

All operating systems require a method of starting up a system from an off to on state. To do so a series of hardware, firmware, and software is executed to bring the device to an operating system. Windows uses BOOTMGR, a file that is installed by default, hidden, and set to read-only access. macOS uses the boot.efi bootloader, which stems from BSD with much of macOS's base code. Linux has multiple boot loaders, and is often chosen by a distribution. The defacto bootloader of Linux is GRUB2 _or GRUB v2_, but there has and will continue to be multiple open source boot loaders available to distribution maintainers and system users.

### The Startup Process

#### Before the Bootloader

The bootloaders sits squarely in the middle of the boot process stack. To get a computer from a powered down state, the first stage is to have the motherboard firmware, either BIOS or UEFI, engage the startup process by providing power and instructions to all components. Next, the stage one bootloader, known as the MBR (Master Boot Record) is pointed to by the firmware and disk. The master boot record is a specified sector of a partition that contains instructions to move to the stage two bootloader. The stage two bootloader is what is referred to when I reference a bootloader. This is the BOOTMGR for Windows, boot.efi for macOS, and primarily GRUB for Linux.

#### The Bootloader

The bootloader then initiates a set of instructions to continue the transition from hardware to software. These instructions are a set of commands designed to define menu entries, disk locations, default options, timeout settings, colors, backgrounds and more. Bootloaders can be defined as both primary and secondary bootloaders, and this is defined by their position on the disk and their use case. A single operating system will always have a primary bootloader located in the MBR, while secondary bootloaders locations are often designated by the partition they are apart of. With a computer using only one bootloader, you may never interact with or even see the bootloader, but when you have multiple operating systems on the same machine proper handshakes between operating systems allow the user to choose the proper operating system.

#### After the Bootloader

Following the bootloader, the system then reaches the kernel. The bootloader starts this operation by executing code to start the kernel and assigns a location in memory for the kernel to operate. The kernel stage loads into memory along with all kernel modules. In Linux, the system is loaded using __intramfs__, or the inital RAM file system. Once in memory, the kernel is able to take hold of the system and operate using the root directory. After the kernel has been loaded, the final stage of startup occurs. INIT, found in /sbin/init starts the appropriate INIT sequence based on the software defined by the distribution or system administrator - see [System and Service Managers](../system-service).

---
#### References

[opensource.com - An introduction to the Linux boot and startup processes](https://opensource.com/article/17/2/linux-boot-and-startup)

[IONOS - What is a bootloader?](https://www.ionos.com/digitalguide/server/configuration/what-is-a-bootloader/)

[Crybit.com - Stages of Linux booting process](https://www.crybit.com/linux-boot-process/)

[Choosing a Boot Loader](https://www.control-escape.com/linux/bootload.html)

[Mac OS X Hints - Restore default bootloader without harming partitions](https://hints.macworld.com/article.php?story=20091111185717745)
