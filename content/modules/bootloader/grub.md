---
title: "GRUB"
date: 2020-12-06T22:13:23-05:00
draft: false
weight: 2
---

```sh
    user@linux:~$ vim /etc/default/grub
```

## The Most Popular Bootloader
Abbreviated as GRUB, the GNU Grand Unified Boot Loader, is the most popular bootloader for Linux, and is often synonymous with the Linux startup process. GRUB is a multiboot bootloader, so it is able to manage and maintain multiple operating systems. When installing a new Linux distribution, GRUB will often pre-load all current operating systems on the disk using the installer for your distribution. When installing a new operating system such as Windows, GRUB may need manual intervention to load a new operating system. GRUB is able to maintain multiple kernels for the same system, and this can be selected at boot.

### Grub and Grub2

GRUB, now known as GRUB Legacy, was replaced by GRUB2. While GRUB Legacy still sees updates to it's development under 0.9x releases, the main project is now GRUB2. GRUB2 is employed in operating systems ranging from Ubuntu to Arch. Notably, the only mainstream version of Linux not using GRUB is Slackware, using LILO primarily with GRUB as a secondary option included in the installation.

### Configuration
When an operating system is installed GRUB is configured during configuration. This process includes adding menu entries to GRUB for the new operating system, as well as any additional operating systems discovered on addition partitions and drives. GRUB customization can occur in multiple formats, depending on installed software GRUB can be customized from a text editor, the terminal, or specialized software such as Grub Customizer, a GUI application.

### Customization
Grub can be edited from both applications and the command line, depending on your use case and preferences. To edit your grub you need to use the file _/etc/default/grub_ by default. When editing this file, be careful as improper changes can cause boot issues that require GRUB recovery or using a boot cd for repair. When you edit /etc/default/grub you can apply changes by running update-grub in the terminal. By parsing grub's configuration file, much of the commands are in plain text allowing users to easily modify timings, default boot procedure, and even the background if you want to add an image. There are additional customization options you can make in the _/etc/default/grub.d/_ directory.

If you would rather use a graphical user interface, which comes with advantages of a simple interface as well as validation on settings, you can use grub-customizer which can be found in most Linux repositories. This application can do many settings an installation disc will do, like finding additional operating systems on the system. grub-customizer is a great tool to use when you are unsure where to start when doing basic modifications to GRUB.

### Recovery
GRUB Legacy and GRUB2 both have GRUB Rescue when GRUB fails to properly load. While this is a scary sight, GRUB developers have made it possible to recover a bootloader using commands, and GRUB can also be restored by using a live CD in the worst case scenario. When a bootloader is unable to locate an operating system, the system will sit in the bootloader, at which GRUB offers a rudimentary terminal to repair itself to get the user back into the Linux. Below is a list of important commands to get you back into the operating system. Once a successful GRUB Rescue occurs, you may need to run `update-grub` to complete the repair.

| GRUB Rescue Command | Definition | Output |
|---|---|---|
| ls | List disks | Shows disks and partitions |
| ls (disk,partition)/grub2 | Search GRUB2 directory on disk | Shows GRUB files on partition |
| grub2-mkconfig | Creates GRUB files | Adds grub.cfg to partition |
| set root=(disk,partition) | Define bootable partition | Set root to /dev/sd\[X] |
| set prefix=(disk,partition)/grub2 | Defines path to partition's GRUB2 files | GRUB2 from OS added to GRUB console |
| insmod \[module] | Insert Module to Linux Kernel | Adds input to Linux kernel for GRUB2 |


---
#### References

[gnu.org - GNU GRUB](https://www.gnu.org/software/grub/)

[slackware docs - Booting](https://docs.slackware.com/slackbook:booting)

[How-To Geek - How to Configure the GRUB2 Boot Loader's Settings](https://www.howtogeek.com/196655/how-to-configure-the-grub2-boot-loaders-settings/)

[Linoxide - Grub Rescue Commands to Fix Boot Issues ](https://linoxide.com/linux-how-to/grub-rescue-commands/)