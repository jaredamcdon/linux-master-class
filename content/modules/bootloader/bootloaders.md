---
title: "Bootloaders"
date: 2020-12-06T22:12:58-05:00
draft: false
weight: 1
---
```sh
    user@linux:~$ sudo update-grub
```
## Current Bootloaders

### History

Bootloaders over the years have varied in functionality, hardware, and software design but all boot loaders have served the same purpose - getting your computer to the operating system. Bootloaders have been designed on motherboards, such as early Macintoshes. DOS and Windows originally used disk segments and structures to load the bootloader from a floppy disk. Linux originally employed a similar solution using the floppy disk. After the floppy disk and proprietary ROM chips on Macintoshes, hardware and software has moved bootloaders towards being directly on the operating system's disk, allowing for modification, relative speed, and simplified installations and boot processes.

Linux, due to its open source nature, has seen a variety of solutions implemented to boot the operating system. After using the floppy disk, prominent bootloaders Loadlin and LILO became popular into the late 90's and early 2000's. Loadlin was similar to modern bootloaders, and lived in the master boot record. LILO, on the other hand was an extension of the DOS bootloader, and required DOS to be on the machine to boot. This is one of the first examples of dual-booting which has been a staple of the Linux operating system. The next bootloader to become mainstream in the Linux world was GRUB, which is still used to this day with modification. GRUB and now GRUB2 are the defacto bootloaders for Linux and maintain high market share across various Linux distributions.

### Popular Linux Bootloaders
| Bootloader | Pros | Cons |
|---|---|---|
| BURG | Visually updated, mouse support, based on GRUB2, modern | Low popularity |
| Floppy Disk | Separate from hard drive | Slow, uses deprecated hardware |
| GRUB | Popular, great documentation | Deprecated |
| GRUB2 | Popular, great documentation, modern, simple, boot from CD | Manual configuration difficult |
| LILO | Fast boot times | Deprecated, requires maintenance, must be in first 8 gigabytes of hard drive |
| Loadlin | Does not affect MBR (Master Boot Record) | Deprecated, requires DOS, manual configuration required |
| Syslinux | Filesystem support | No multi-filesystem boot |
| Systemd-boot | UEFI support | Requires secondary bootloader |

---
#### References

[TecMint - 4 Best Linux Boot Loaders](https://www.tecmint.com/best-linux-boot-loaders/)

[Independent Software - OPERATING SYSTEM DEVELOPMENT BOOT SECTOR STRUCTURE](http://www.independent-software.com/operating-system-development-boot-sector-structure.html/)

[Choosing a Boot Loader](https://www.control-escape.com/linux/bootload.html)