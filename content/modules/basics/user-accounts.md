---
title: "User Accounts"
date: 2020-11-12T22:43:08-05:00
draft: false
weight: 5
---
```sh
    user@linux:~$ useradd LinuxMaster
```
## Users
In Linux there are three types of user accounts. The root account provides full control to the user and is treated as an administrator, The 'standard' user account has privileges based upon ownership and can do basic tasks. Service accounts are accounts not accessed by users, but by services on the system. Service accounts permission structure is similar to a traditional user account, having basic access and additional permissions based upon group and user permissions.

>To show users you can use the cat command on the /etc/passwd file
```sh
    user@linux:~$ cat /etc/passwd
```

### Standard User Accounts
All installations of Linux require at least one standard user account. This account will have default permissions, owning files in their home directory and this account has access to basic directories and programs defined by file and directory permissions.

Standard user accounts gain permission from administrator intervention. Administrators are able to grant individual users owners of files and directories, as well as modify what groups a user are apart of, granting additional privileges.

Standard user accounts are able to customize their Linux use through "dot files" in their home directory. These files are named dot files due to the naming scheme uses, for example .bashrc is a dot file. These files contain user configuration, such as default text editors, set default paths and more. Dot files are by default hidden, and to view dot files in a terminal you often need to add the -a flag, for example, `ls -a` will use the ls command and the -a flag will show all files. 
### Root Account
A root account is similar to a standard user account, but with additional privileges. Root accounts can be seen as _administrator_ accounts, having full access to the system. Root accounts are added to all installations of Linux.

To gain temporary access to the root account, regular users with _sudo_, _wheel_, or similar groups depending on the distribution, can add `sudo` before executing their command. sudo, or Super User Do, allows access to root privileges, making the need to log into the root account often unnecessary.
### Service Accounts
Service account permissions work the same as standard users, but are not designed to be logged into by users. Service accounts are installed by programs to execute functions. For example, sshd is both an program and a service account on most installations of Linux. The sshd account allows the process to run in the background, and process incoming users using secure shell to remote into the operating system. It is possible to log in to service accounts as a user, but is not recommended and may cause permission issues if improperly managed.
## Groups
Groups in Linux contain additional privileges to users based upon affiliation and group ownership of files and directories. Unlike users, groups do not have configuration files or home folders, limiting the uses of the group account. A group account will give you permissions for certain programs, such as sudo, as well as access to directories and files. Group management in Linux is often done by organizational structure or permissions-based roles.
## Managing User Accounts
Managing accounts in Linux is similar to other operating systems. Linux uses two user management systems, centralized and standalone. Standalone user management occurs at the individual system. This is the default method, and relies on databases run on the host system for authentication and validation. Centralized user management is performed at an organizational level, and uses authentication from an external server. This external server uses some form of active directory to store user and group permissions so that an account can be accessed on multiple computers on a network.

In most Linux installations, and most all standard configurations, Linux relies on standalone user management and does not contact an additional server to provide authentication. Organizations can configure Linux management processes at their discretion, and active directory is not as popular of a feature as seen on Windows Server Active Directory Domain Services.

---
#### References

[ComputerNetworkingNotes - Types of Users in Linux Explained with Accounts](https://www.computernetworkingnotes.com/rhce-study-guide/types-of-users-in-linux-explained-with-accounts.html)