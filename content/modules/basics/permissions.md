---
title: "Permissions"
date: 2020-11-12T22:43:14-05:00
draft: false
weight: 6
---
```sh
    user@linux:~$ chmod 755
```

## The Users
Permissions in a system declare what rights users have and define the rights given to said users. Linux manages permissions to users in three ways. The global __User__ defines access to everyone on the system and is the first check. The __Group__ is the group defined to own the content. This group could be an organizational definition, such as the accounting team, or a permissions-based definition, such as the _sudo_ or _wheel_ groups. The group role allows additional access above the user role to members of a predefined group. The last check of permissions is the __Owner__. The owner holds user rights, often group rights, and any additional rights defined by the owner role. Only one user can be the owner, and only one group may be the group of a file or directory. When defining rights, a shorthand of ugo - User Group Owner - can be used.

| Name | Shorthand | Definition | 
|---|---|---|
| Owner | o | The owning individual of the file/directory |
| Group | g | The group who owns the file/directory |
| User | u | Anyone on the system |

## The Permissions
### Overview
Linux defines rights by three categories, namely read, write, and execute. These building blocks contain all forms of access an object can have, and be used in combination to declare multiple rights. These rights are self defining and grant different permissions based upon if they are defined for a file or a directory. The __Read__ permission allows users to read or view contents of a file, or view file names of a directory. The __Write__ permission declares permissions to modify or delete files, and the ability to create, modify, or delete files when applied to a directory. The __Execute__ permission allows a file to be executed (or run), and execute when applied to a folder allows execution of commands on a folder, such as listing contents of or changing into directories.

### Octals and Shorthands
Permissions are not added through commands such as ~~`grant joeLinux read write to testFile`~~ but look more like `chown joeLinux:admin testFile && chmod 654 testFile`. Breaking down the command, `chown joeLinux:admin testFile` grants the user joeLinux ownership and sets admin as the owning group of the file testFile. `chmod 654 testFile` grants the owner read and write access (6), the owning group read and execute access (5), and the remaining users read access (4) to the testFile. Octals are shorthand commands to grant user permissions and work through addition. The read permission holds a value of 4, write a value of 2, and execute a value of 1. Using addition, if you wanted to give write and execute, the shorthand is 3, 2 + 1. Giving all access is 7, 4 + 2 + 1. No access is a 0, representing none of the permissions. 

Permissions can also be added, removed, and defined using an additional shorthand. Converting `chmod 654 testFile` to the defining shorthand, you can define permissions as `chmod o=rw, g=re, u=r testFile`. While two ways to define access may seem redundant, using the ugo shorthand allows for additional functionality, adding and removing permissions. Adding and removing permissions can be done without defining all permissions, making is easy to add or remove functionality without causing accidental error to other rights. Using `chmod u+x`, you are able to grant users the execute permission. With `chmod g-w` you are able to remove the owning group's write access.

>Octal Calculation and ugo Shorthand
| Name | Shorthand | octal | Permission |
|---|---|---|---|
| Read, Write, Execute | rwx | 7 | All permissions |
| Read | r\-\- | 4 | Read/view |
| Write | -w- | 2 | Create, modify, delete |
| Execute | \-\-x | 1 | Execute/run |
| None | \-\-\- | 0 | No permissions |

>Operator Commands
| Operator | Definition |
|---|---|
| + | Add permission |
| - | Remove permission |
| = | Set permission |

---
#### References

[Chmod Calculator](https://chmod-calculator.com/)

[Red Hat - An introduction to chmod](https://www.redhat.com/sysadmin/introduction-chmod)

[tutorialspoint - Unix/Linux - File Permission/Access Modes](https://www.tutorialspoint.com/unix/unix-file-permission.htm)