---
title: "File Structure"
date: 2020-11-12T22:42:57-05:00
draft: false
weight: 3
---
```sh
    user@linux:~$ cd ~/../LinuxMaster/Documents/file-structure
```
## File Management and Design
Compared to Windows, UNIX based operating systems including Linux and macOS manage files using drastically different structures. While Windows mounts drives individually using letter names, Linux places the entire file system under directories at the root (/). Applications and program files have best practices for locations, but are not held to a strict policy, causing some variance between Linux distributions, package managers, and application types.

### Basic Syntax
>Linux uses methodologies that are not seen across other operating systems and requires some high level understanding for file system proficiency.


* __Files__
    * Anything that contains data as a document or executable binary.
    * Examples: file.txt, app.py, presentation.pptx, index.html
* __Directories__
    * Known as _folders_ in Windows
    * contain files and other directories
* __Links__
    * Logical or symbolic references to files and directories in other locations
    * Examples: A Documents/ folder located in /home that points to a separate disk

### Default Directories
>*based off of Ubuntu's file structure


| Location | Name | Usage |
|---|---|---|
| / | root | Parent directory of file system |
| /bin | binaries | Contains executable utilities |
| /boot | boot | Contains files needed to start operating system |
| /dev | devices | Contains physical and logical devices; i.e. keyboard |
| /etc | et cetera | Contains configuration files and tables; i.e. fstab | 
| /home | home | Contains individual user directories |
| /lib | libraries | Contains kernel modules and drivers |
| /lib64 | libraries 64 | Libraries folder for 64-bit systems. |
| /media | media | Contains removable devices such as USB, DVD |
| /mnt | mount | Contains filesystem mount points; i.e. extra disks |
| /opt | optional | Contains software not included in installation |
| /proc | processes | Contains runtime system information; i.e. memory |
| /sbin | system binaries | Contains utilities to start, maintain, recover system |
| /root | root home | Contains /home style directory for root user |
| /srv | server data | Contains data for services provided by system |
| /tmp | temp | Contains temporary files, cleared at boot |
| /usr | user | Contains non-critical shared executables and resources |
| /var | variable | Contains data such as printer queues and web files |
| /swap | swap | Contains spare virtual memory for paging, hibernation |

---
#### References
[Dell - The types and definitions of Ubuntu Linux Partitions and Directories Explained](https://www.dell.com/support/article/en-us/sln152018/the-types-and-definitions-of-ubuntu-linux-partitions-and-directories-explained?lang=en)

[The Linux Juggernaut - /etc explained](https://www.linuxnix.com/linux-directory-structure-explainedetc-folder/)

[Linuxtopia - Linux Filesystem Hierarchy](https://www.linuxtopia.org/online_books/linux_beginner_books/linux_filesystem/)
