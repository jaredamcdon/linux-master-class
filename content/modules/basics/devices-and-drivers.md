---
title: "Devices and Drivers"
date: 2020-11-12T22:40:49-05:00
draft: false
weight: 4
---
```sh
    user@linux:~$ ls /dev/*
```
## The /dev Folder
The Linux file structure holds directories and files for devices such as keyboards, memory, printers, and more. Linux interacts with devices through passing data in the devices defined files. These files contain both text and block data, which is defined by the type and use case of the device.

## Device Classification
>Linux devices can be classified into block and data stream

#### Block Devices
Block type devices handle data in blocks in comparison to the text data handled in data stream. An example of a block device, are disks, which handle data in sets of 256 bytes. The easiest way to tell what a block device is running `lsblk`, or list block devices, in a terminal. The output of this file shows hard disks, flash drives, CD-ROMs, and similar devices.

#### Data Stream Devices
Data stream devices are more common than block devices. Data stream primarily refer to the stream of text data between devices, files and the shell. Data is unidirectional in these files. Data streams can be looked at as a flow of pipes. You can use a data streams to create an input and send that to an output, such as redirecting a command output into a file as an input. Treating data as files allows the easy flow of information.

Naming Conventions:
* __Input streams__
    * stdin
        * Standard input stream: Text input
* __Output streams__
    * stdout
        * Standard out: Text from the shell
    * stderr
        * Standard error: Command error messages

## Drivers in Linux
Drivers in linux holds the jobs of allocation, initialization, declaration and registration of devices. Device drivers need allocating as they are statically defined, and need a permanent location to be accessed by the shell. Initialization of drivers creates the name, bus fields, and often the devclass. The process of initialization allows for drivers to properly communicate with the kernel. Driver declaration defines the names and functions of a driver. Registration of drivers define the structures the device uses. Drivers in Linux are seldom interacted with by the consumer, but a high level understanding can help explain the process of how communication occurs between the terminal and the kernel.

---
#### References
[The Linux Kernel - Device Drivers](https://www.kernel.org/doc/html/v5.4/driver-api/driver-model/driver.html)

[mjmwired.net - devices.txt](https://www.mjmwired.net/kernel/Documentation/devices.txt)

[Opensource.com - Managing Devices in Linux](https://opensource.com/article/16/11/managing-devices-linux)

[Opensource.com - Linux Data Streams](https://opensource.com/article/18/10/linux-data-streams)

[How-To Geek - What Are stdin, stdout, and stderr on Linux?](https://www.howtogeek.com/435903/what-are-stdin-stdout-and-stderr-on-linux/)

[LinOxide - Command to List Block Device in Linux](https://linoxide.com/linux-command/linux-lsblk-command/)