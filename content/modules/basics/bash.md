---
title: "Bash"
date: 2020-11-12T22:41:00-05:00
draft: false
weight: 7
---
```sh
    user@linux:~$ bash --help
```
## What is Bash?
Bash is both the shell and scripting language of Linux and macOS, and is able to be installed on POSIX compliant operating systems such as BSD derivatives. When you interface with a linux terminal, you are using Bash, the Bourne-Again Shell. While linux is able to use multiple different shells at the command line, bash is by far the default and most popular option.

### The Bourne-Again Shell
When you open a terminal, you are greeted by your name and location. This is configured using bash and all commands you enter are interpreted within bash. Changing your thinking from a graphical user interface to a terminal can be challenging, but graphical applications do the same process in reverse. When a command is inputted in the terminal, it is interpreted and that command interacts with the kernel. Using a shell is how a human can directly interact with a computer.

#### History of the Bash Shell
Bash was released in 1989, but has roots in 1977 at the release of sh _Bourne Shell_ created by Stephen Bourne. The shell had the goal of being an interpreter for both direct interaction and scripting. The Bourne-Again Shell was designed to replace the Bourne Shell created by Brian Fox with GNU. The Bash shell added important features such as environment variables while also providing backwards compatibility with sh.


## Bash the Scripting Language
Bash as a language functions just as Bash in the terminal. Using the same commands and saving them as scripts, developers are able to increase their efficiency on the terminal by running a script rather than procedurally typing a set of commands. When scripting in bash, you can use the _shebang_ to declare what interpreter you want the system to use to execute the commands. This works similarly to how many system administrators write Python scripts, declaring which version of Python the application is written in to prevent errors at runtime. With Bash being an interpreted language, Bash requires no compiling to binary,

## Common Bash commands

| Command | Function |
|---|---|
| ls | List directory contents |
| pwd | Print working directory |
| cd | Change directory |
| echo | Display text |
| grep | Find pattern |
| locate | Locate file/directory |
| cat | Concatenate and print files |
| less | View contents of file |
| touch | Create file/update timestamp |
| mkdir | Make new directory |
| man | Manual of ... |
| mv | Move/rename directory |
| cp | Copy file/directory |
| rm | Remove file/directory |
| rmdir | Remove empty directory |
| chmod | Change file/folder permissions |
| \| | Pipe output |
| kill | Kill process |

---
#### References
[cs.lmu.edu - Introduction to Bash](https://cs.lmu.edu/~ray/notes/bash/)

[IBM Developer - Evolution of shells in Linux](https://developer.ibm.com/technologies/linux/tutorials/l-linux-shells/)

[Medium - Bash Scripting](https://medium.com/sysf/bash-scripting-everything-you-need-to-know-about-bash-shell-programming-cd08595f2fba)

[educative - Bash cheat sheet](https://www.educative.io/blog/bash-shell-command-cheat-sheet)