---
title: "File Systems"
date: 2020-11-12T22:40:25-05:00
draft: false
weight: 1
---
```sh
    user@linux:~$ lsblk
```
## The Filesystems of Linux
Linux has the ability to read and write to most all filesystems, if only requiring an additional package to do the leg work. Linux comes with a variety of options with popular options changing over time to best suit the needs of the community. Linux often defaults to the Ext Extended File System, JFS Journaled File System, XFS, and Btfrs B-tree file system. These standard options come with high reliability and are great for most all use cases. Linux also supports the ability to read and write NTFS and FAT filesystems seen on Windows.

### Ext
The Extended File System is currently the most popular file system used on Linux, EXT is the default for Debian, Ubuntu, CentOS, and many more distributions. Ext was designed specifically for Linux and has been in use since 1992 over multiple generations. Ext4 is the current implementation, and was released in 2008.

|Version | Release date | Max File Size | Max File System Size |
|---|---|---|---|
| Ext | 1992 | | 2GB |
| Ext2 | 1993 | 2TB | 32TB* |
| Ext3 | 2001 | 2TB | 32TB* |
| Ext4 | 2008 | 16TiB | 1EB |
>*Actual limit approximately 16 TB 

### JFS
JFS was originally designed for IBM AIX OS and OS/2 operating systems. Journaled File System is an open source file system used by IBM, but sees very little use in consumer and desktop applications. It was designed for stability in server use cases. JFS can support files up to 4 petabytes and file systems of 32 petabytes. 

### XFS
XFS Was added to the Linux kernel in 2001, and was created by Silicon Graphics, Inc in 1993. While a largely underused, XFS is the primary file system Red Hat Enterprise Linux 7 and 8. Red Hat uses additional applications to support backup and restore features specific to XFS. XFS is a 64-bit file system with a maximum file size of 8 exabytes and supports online de-fragmentation and uses metadata journaling to enable faster data recovery after system crash.

### Btfrs
B-tree file system, also known as Btrfs or Butter FS, is a relatively modern file system for Linux. Development for Btrfs started in 2007 and has been apart of the Linux kernel since 2013. Btrfs brings many new features to file systems such as snapshots, checksums and more. Btrfs has not yet seen widespread adoption due to lack of reliability, one of the pillars of the Linux operating system. In Red Hat Enterprise Linux Btrfs was dropped from support.

---
#### References
[Partition Wizard - Linux File System](https://www.partitionwizard.com/partitionmagic/linux-file-system.html)

[Linuxreviews.org - File System Guide _2011_](https://web.archive.org/web/20110917065413/http://linuxreviews.org/sysadmin/filesystems/)

[Nongnu.org - Ext2](https://www.nongnu.org/ext2-doc/ext2.html)

[Techspirited - File Systems Ext2 Ext3 Ext4 Explained](https://techspirited.com/file-systems-ext2-ext3-ext4-explained)

[Geek University - JFS File System](https://geek-university.com/linux/jfs-file-system/)

[Red Hat Documentation - The XFS File System](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/storage_administration_guide/ch-xfs)