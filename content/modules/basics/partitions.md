---
title: "Partitions"
date: 2020-11-12T22:40:37-05:00
draft: false
weight: 2
---
```sh
    user@linux:~$ sudo parted
```
## Disks and Operating Systems
All operating systems partition disks to manage storage for the system configuration, programs, and user files. Linux, Windows, macOS and all operating systems need to use disk partitioning to initialize and define storage, and this process is done on hard drives, solid state drives, flash cards, USB drives, and any form of storage media. Operating systems can use one or multiple disks and handle this data differently, and this has to be manually configured by the user or manufacturer, even with easy setup utilities.

### Partitioning
During the operating system installation, users will be required to initialize and format partitions. This allows the user to choose how drives are interacted with, and how to partition the drives. Partitions fall into one of three categories: primary, logical, and extended. There is a maximum of four primary partitions, with extended partitioning using one of the four partitions if extended partitioning is chosen. An extended partition acts as a pool or container to hold logical partitions. The rule of thumb is if four or less partitions are used, set all as primary. If there are more than four partitions, put essential partitions such as operating systems and boot information on primary partitions, and all remaining non-essential data can fall under logical partitions housed in an extended partition.

>Partition Types
| Partition | Usage | Limitation |
|---|---|---|
| Primary | Required | Four maximum partitions |
| Logical | Additional partitions | Can not be booted from* |
| Extended | Logical drive pool | Uses primary partition slot |

*with certain bootloaders

### Linux Partitioning
Linux partitioning can be done during and after installation. During installation, partitioning is often done through parted or gparted. These applications handle partitioning, with the distinction being gparted is _graphical_ while parted is a terminal application. There are other applications, and fdisk is a popular alternative to parted. Partitioning in Linux can be as simple as a single root (/) folder, or you can have separate partitions for /home, /boot, /mnt/* and whatever else you want to keep separate from the primary disk. This depends on both hardware available and the software the operating system is planned for. For example, Windows Server best practices is to keep application instances and data separate from the C, or primary drive.

### Swap
What is swap? Do I Need swap? Swap is a page file, or a disk location to store overloaded RAM (Random Access Memory). You may or may not need swap, if you're unsure, it never hurts. Swap is mounted with partitioning and lets your RAM move processes that are not currently active to keep your system running and functioning properly. Swap is mounted as /swap and is created using partitioning software. An outdated rule of thumb is to allocate 150 percent of your RAM as swap. This rule of thumb would allocate six gigabytes of swap for four gigabytes of RAM. This ideology has flaws in modern computing. Disk size has greatly been decreasing since the transition to cloud storage and solid state drives. Memory has been increasing due to the decrease in cost and increase in amount of memory per RAM module. Modern computers can easily hold 32 gigabytes of RAM, and that would required 48 gigabytes of swap space, one third of a standard 120 gigabyte solid state drive.

Modern swap usage due to external factors in the hardware realm make defining swap space difficult. For most modern desktops with average use cases, 16 gigabytes or more of RAM often will not need /swap space, and computers with less than 16 gigabytes of RAM should mirror storage one to one. Note, this is not a rule, and if you are unsure and have the storage space, 150% of memory storage is almost always adequate for swap space.

### Multi-disk Management
Linux and UNIX derived operating systems handle drives in a different methodology thank DOS-based operating systems. While DOS-based (Windows) operating systems mount drives by drive letters, Linux offers much more freedom and choice. Multiple drives are often mounted under the /mnt directory, but can be delegated as best seen fit. An example of separate disk mounting could be shown in the /home drive to store user data on. If user data is to be stored on a separate drive, when partitioning the operating system, you would mount the separate drive - example: sdb to /home. This would leave all of the primary drive - example: sda to hold the kernel, drivers, applications, and additional data without having to maintain storage space for the /home directory.

---
#### References
[How-To Geek - Hard Disk Partitions Explained](https://www.howtogeek.com/184659/beginner-geek-hard-disk-partitions-explained/)

[Ultimate Linux Newbie Guide - Partitioning A Disk](https://linuxnewbieguide.org/overview-of-chapters/more-advanced-guides/partitioning-a-disk/)

[EaseUS - Logical vs Primary | What's the Difference and Which is Better](https://www.easeus.com/partition-master/logical-vs-primary.html)