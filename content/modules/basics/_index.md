---
title: "Basics"
date: 2020-11-12T22:38:49-05:00
draft: false
weight: 3
---

## Overview
While Linux currently shares approximately one to three percent of the desktop market share, Linux and its derivatives are much more popular elsewhere. Using specialized versions of Linux, you can see it in Android, Internet of Things devices, Chromebooks, and most important, servers. While Windows holds the highest market share for servers, UNIX systems - which are primarily Linux - hold approximately seventy percent of website server hosting.

### Building Blocks
Linux, its programs, its file structure, and a great deal of the original kernel is built upon UNIX. This defines how devices interact with the computer, where applications and user files are stored, and how to mount and access memory and storage. What makes Linux the defining choice for many is the documentation and troubleshooting. While the Linux community is much smaller than both macOS and Windows, the communities and documentation are often stronger due to both the open-sourced community based design and the relative simplicity to interface with the operating system directly.

### Freedom
Linux is free. Due to the GNU GPL, Linux always falls under free as in libre - open to all. Many of Linux's distributions are also free as in free beer. There is rarely a license cost, which makes Linux a great option for consumers and businesses to use Linux how they would like. There are no costs per core or per virtual machine, as seen in Windows. Linux is also not hardware dependent such as macOS. This gives users not only the ability to download and try most versions of Linux at no cost, but also gives the full rights to read and modify the code to add or remove any features as seen fit.

---
#### References
[NetMarketShare - Linux](https://www.netmarketshare.com/linux-market-share)

[W3Techs - Usage Statistics of Operating Systems for Websites](https://w3techs.com/technologies/overview/operating_system)