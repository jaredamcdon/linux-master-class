---
title: "Maintenance"
date: 2020-11-30T10:44:39-05:00
draft: false
weight: 2
---
```sh
    user@linux:~$ uname
```

## Overview
The Linux kernel requires very little moderation from a system user or system administrator. Designed to be forgotten, the Linux kernel handles a computer's drivers and low level processes. The weight of the kernel's code makes manual editing, while possible, proves difficult and most kernel maintenance is done through kernel upgrades. All Linux distributions provide the opportunity to upgrade the kernel, but depending on the release structure and maintainers looks different. Arch-based systems will upgrade the kernel when a new kernel is pushed to the repository, where more stability focused distributions such as Debian and derivatives do not upgrade from the standard package manager. As a rule of thumb with much of technology, if you do not see any performance or stability concerns, it may be best to leave the kernel as is.

### Common kernel commands
When doing maintenance around the kernel, there are useful commands that can help your information gathering and troubleshooting. The `uname` command with appropriate flags can tell you all about a systems kernel information. Flags such as -a for all or -r for release or -o for operating system can help system administrators better understand the underlying architecture of a computer. Inside the /proc folder, you can use `cat /proc/version` to get an output of the current kernel as well as other basic information. `dmesg` can also be used to inspect a kernel. The dmesg command is defined as a command to "print or control the kernel ring buffer." When Used in conjunction with grep, you can sort the information pertaining the the Linux kernel, such as `dmesg | grep Linux`. 

### Installing a New Kernel
When a kernel upgrade is necessary, you can do so in many ways depending on what Linux Distro you are running. For Ubuntu and many systems, installing a new kernel comes from downloading the package online and running `dpkg -i *.deb` in the proper directory. After installing, reboot the system and using the uname command you should see the new kernel version on your system.

----
#### References

[It's FOSS - 3 Ways to Check Linux Kernel Version in Command Line](https://itsfoss.com/find-which-kernel-version-is-running-in-ubuntu/)

[It’s FOSS - Upgrade Linux Kernel Ubuntu](https://itsfoss.com/upgrade-linux-kernel-ubuntu/)