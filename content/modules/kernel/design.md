---
title: "Design"
date: 2020-11-30T10:44:13-05:00
draft: false
weight: 1
---

```sh
    user@linux:~$ cat /dev/urandom
```

## Operating System Kernel Design
When designing, supporting, and implementing applications at a low level, understanding the design of a systems kernel allows for better management of the application's use of system resources. The kernel is a layer that sits between user processes and physical hardware to manage the system and user processes. The Windows micro kernel is designed with segmentation and security in mind, and can be compared to a Virtual LAN on a switch for network administrators - keeping services from directly interacting with each other. The monolithic kernel employed by Linux in contrast does not use segmentation and allows kernel subsystems to interact directly with each other. Processes of a monolithic kernel can and often are divided into subsystems, but are not required to do so.

### Micro Kernel
The name micro kernel comes from only essential kernel services running as a process of the kernel space, with additional kernel processes running in the user space. Micro kernel design allows for a more modular design in the overall architecture of an operating system. This design allows for a theoretically more secure kernel with additional stability through rogue processes not affecting other kernel processes.

### Monolithic Kernel
The monolithic kernel design, employed by Linux, contains all kernel processes under the same umbrella. While the monolithic kernel is not defined by a modular approach, many of the core modules can be enabled and disabled such as a micro kernel does. The monolithic kernel works by usage of address spaces, keeping all kernel processes in the same address space, while each application receives its own virtual address space.

### Four Roles of a Kernel
Kernels, whether monolithic or micro, maintain key processes to keep a system healthy and functioning. These four components include memory management, process management, device drivers, and systems calls and security. These individual components are treated as kernel subsystems and are developed by separate teams and managed by one high level team to ensure overall kernel performance. The roles of a kernel are not discretely defined, and while this guide references four key components of a kernel, this can be broken down into more or less processes as defined by the maintainers discretion.

#### 1. Device Drivers
The Linux kernel uses device drivers to manage the system as a whole, taking block and data stream inputs and outputs and converting them to data structures to be understood by the system. Device drivers in the kernel help reduce code duplication and make drivers as simple as possible for both maintainers and the computer running the processes. This data is used to understand how a device is attached, how the device is operating, and how to manage the data inputs and outputs of the device. The kernel subsystems for devices drivers also handles device discovery and removal. 

#### 2. Memory Management
Memory management is the process of tracking, storing, and maintaining the memory of a computer. The kernel subsystem for memory management allocates and frees memory when processes have changes in memory requirements. Memory management also controls paging and swapping memory. This dynamic process moves memory from RAM (Random Access Memory) to predefined disk space to allow a system to continue running even when hardware memory limitations have been reached, and allow computer systems to continue operating when RAM usage is full, or even allow processes made by the user, such as hibernation. Memory management subsystems also address memory for use the in the user space, such as for applications.

#### 3. Process Management
Process management pertains to utilization of the CPU (Central Processing Unit). This decides when, where, and how much of the CPU a process should use. Linux breaks these processes down into _tasks_, which allocate scheduling. Process management uses core low-level computer science tools, such as the pointer to decide where and how to allocate management of processes.

#### 4. System Calls and Security
System calls and security work with requests from other services and manage those requests. System calls are discrete commands that are interpreted by the kernel. These calls manage I/O (input/output), networking, processes, and security. These can be seen as scripts run by processes that can create new processes, manipulate files and devices, manage security of files, and communicate between domains of a computer system.

----
#### References

[Red Hat - What is the Linux Kernel?](https://www.redhat.com/en/topics/linux/what-is-the-linux-kernel)

[The Linux Kernel - Introduction](https://linux-kernel-labs.github.io/refs/heads/master/lectures/intro.html)

[Geeks for Geeks - Introduction of System Call](https://www.geeksforgeeks.org/introduction-of-system-call/)