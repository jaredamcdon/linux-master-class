---
title: "Development"
date: 2020-11-30T10:47:54-05:00
draft: false
weight: 3
---

```sh
    user@linux:~$ git pull
```

### Over 70,000 Files and 29,000,000 Lines
The Linux kernel is the number one open source project, aided by the help of approximately four thousand developers and over four hundred companies. Linux kernels are released every 67 days, with one kernel gaining LTS (Long-Term Support) every year. With developers all over the world contributing, Linux kernel development happens around the clock with constant additions, modifications, and deletions to the code base being monitored using git, the preferred source control tool of the Linux team. The changes are looked over by teams of maintainers who will continually push the changes up the chain of command until they reach Linus Torvalds, the top code manager and maintainer. Kernel changes move through this chain of command to keep the mission unified and to have multiple eyes checking the content and quality of code pushed to new releases.

### The Contributors of the Kernel
The Linux kernel is developed and maintained by passionate software engineers, maintainers, and corporations. This work is primarily done by corporations, as unpaid developers make up currently less than ten percent of the code base. This leaves most of the kernel work to be cone by corporations. While often seen as a competitor, Microsoft contributed the fifth most code of any company to the Linux 3.0 kernel. According to a 2016 report, the top contributors in descending order was Intel, Red Hat, Linaro, Samsung, and SUSE. This comes with Intel contributing approximately thirteen percent and SUSE contributing approximately three percent. These companies all have a vested interest in the future of Linux, with companies like Intel and Samsung being hardware vendors, and Red Hat and SUSE generating profit directly from their release and support of Linux.

### Kernel Composition
Five percent. The core of the Linux kernel is five percent of the Linux kernel code. The majority of the kernel, at fifty-five percent of the code is drivers. The drivers used to interface between kernel apis and hardware are now the majority of the code. This continues to evolve as Linux gaines more support, more usage, and hardware continues to evolve.

----
#### References

[ZDNet - Linus Torvalds on the future of Linux kernel developers and development](https://www.zdnet.com/article/linus-torvalds-looks-at-the-future-of-linux-kernel-developers-and-development/)

[gregkh - Presentation on how the Linux kernel is developed](https://github.com/gregkh/kernel-development)

[ZDNet - Top Five Linux Contributor: Microsoft](https://www.zdnet.com/article/top-five-linux-contributor-microsoft/)

[The New Stack - Who Contributes to the Linux Kernel?](https://thenewstack.io/contributes-linux-kernel/)