---
title: "Kernel"
date: 2020-11-29T16:05:48-05:00
draft: false
weight: 4
---

## Overview
The __kernel__ is an essential component of all operating systems. Designed to manage communication between applications as well as system services, modern operating systems require a kernel to navigate system resources, manage processes, and keep computer systems running. Modern operating systems rely on one of two kernel architectures largely based upon common ancestors. Windows uses a _micro_ kernel, while UNIX derived systems, such as Linux, operate using a _monolithic_ kernel. These differences not only treat applications and services differently, but can also change how a system is updated, requiring Windows to update during restarts while linux largely does not require restarts with updates.

### Privileges and Responsibilities
Kernels and applications operate using different sets of permissions, and these can vary from system to system. Kernel operations are able to run with higher privileges while applications are limited on their ability to increase security and stability. Kernel operations are able to perform operations that regular applications can not and also kernels have a lower level access to hardware such as memory and processor access. These processes are divided into two categories, the Kernel Space and the User Space.

The kernel also has additional jobs required to keep a system running. These can be broken down into 4 key components:
>1. Memory Management
>2. Processor Management
>3. Device Drivers
>4. System Calls and Security

---
#### References

[Red Hat - What is the Linux Kernel?](https://www.redhat.com/en/topics/linux/what-is-the-linux-kernel)

[The Linux Kernel - Introduction](https://linux-kernel-labs.github.io/refs/heads/master/lectures/intro.html)

[IBM Developer - Anatomy of the Linux Kernel](https://developer.ibm.com/technologies/linux/articles/l-linux-kernel/)

[O'Reilly - An Overview of Linux Kernels](https://www.oreilly.com/library/view/understanding-the-linux/0596002130/ch01s06.html)