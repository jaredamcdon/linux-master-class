---
title: "Labs"
date: 2020-12-05T17:34:23-05:00
draft: false
weight: 3
---

## Navigation
---
### [Install Linux]()
> Install Ubuntu on a virtual machine and with Windows Subsystem for Linux

### [Modify File Permissions]()
> Using UGO permissions in the command line

### [Working with Users and Groups]()
> Create and manage users

### [Creating Symbolic Links]()
> Connect directories and files in different locations

### [Formatting Partitions]()
> Working with disks in the command line and with a GUI

### [Make a Bash Script]()
> Using bash to automate

### [Install a GUI]()
> Add a graphical user interface to Ubuntu Server

### [Upgrade your Kernel]()
> Managing new kernel installations

