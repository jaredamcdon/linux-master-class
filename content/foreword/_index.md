---
title: "Foreword"
date: 2020-12-06T12:34:40-05:00
draft: false
weight: 1
---

### Why I wrote Linux Master Class
Writing a body of work does not happen overnight, and neither does the sparks that ignite the writing process. I am a student in college, as well as a programmer and daily linux user for twelve years. My university has a great program for future system administrators, but has lacking knowledge base and resources to teach the ins and outs of Linux. From a practitioner's standpoint, many colleges teaching information technology do a great job. From the perspective of deeper understanding and academic knowledge, the best research you can do is in the workforce. After doing a lab in school doing basic step-by-step tasks, I decided to try my hand at writing my own Linux manual. This writing is the product of my dream to better the intermediary learning of Linux - the stages between complete beginner and expert.

### My Background
Starting with Linux Mint in the late 2000's, I learned Linux for the sake of discovery. I have been an intermittent Linux user ever since, and I have daily driven Linux for the over two years. I currently run Linux on my desktops, my servers, and even one of my phones. Linux has given me the opportunity to learn about the open source community, take ownership of my desktop, run countless server applications for my family, and has opened doors for several careers in my short time on this earth. I have experience from Ubuntu to CentOS to Arch, and currently employ primarily Debian and Debian derivatives. At work my team uses CentOS, and as a developer I use open source software such as git and python on a daily basis.

### My Writing Process
With over ten years of experience in Linux, all topics and works in this body of work are my own, with supplemental resources helping me make sure I am providing accurate and holistic information to the readers. After deciding the topic and subtopics I choose to write about, I do a deep dive on the subject to find the most pertinent information from an academic and practical perspective. This work is by no means an all encompassing guide, but gives a strong foundation with all resources crediting for further learning. I hope I am able to spark further research by readers and hope my transparency of resources can provide the best information possible.