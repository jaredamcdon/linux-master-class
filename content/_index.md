---
title: "Home"
date: 2020-11-09T22:18:24-05:00
draft: false
---
# Linux Master Class
>The how and why of the Linux operating System
---

## Philosophy
>Why a Linux Doc site?

Linux Tutorials suffer from being too simple or too complicated.

## Where to start?

Go to the [Modules Tab](./modules/) to get started!
```sh	
	user@linux:~$ echo 'Hello, Linux!'
	Hello, Linux!
```
![Linux Word Cloud](/linuxWordCloud.png)